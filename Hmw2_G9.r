rm(list=ls())

# Libraries:
library(dplyr)
library('geoR')
library(sp)
library(gstat)
library(scatterplot3d)
library(akima)
library(ggplot2)

# Data
load("./data/quota32.Rdata")
load("./data/datiAll.RData")


dati1973 <- dati %>%
  filter(anno =="1973") 

dati1978 <- dati %>%
  filter(anno =="1978") 

dati1981 <- dati %>%
  filter(anno =="1981")


### Question 1: Exploratory analysis 

#---------------------

# Let's explore the different regions : where are the stations available, by year ?


# Basilicata
dati_basilicata1973 <- dati1973 %>%
  filter(Nome_Regione=="Basilicata") 
dati_basilicata1978 <- dati1978 %>%
  filter(Nome_Regione=="Basilicata") 
dati_basilicata1981 <- dati1981 %>%
  filter(Nome_Regione=="Basilicata") 

plot(dati_basilicata1973$XUTM,dati_basilicata1973$YUTM,pch=20,xlab="x",ylab="y", col="green")
lines(Bas$X,Bas$Y)
title(main="Localisation of the rainfall data in the Basilicata region") 
points(dati_basilicata1978$XUTM,dati_basilicata1978$YUTM, col="red")
points(dati_basilicata1978$XUTM,dati_basilicata1978$YUTM, col="blue",pch=3)
# The rainfall data seems (globally) uniformly distributed all over the region Basilicata.
# We can notice that all the 1978 and 1981 stations were already there in 1973, but there is a station of 1973 left in 1978/1981.


# Calabria
dati_calabria1973 <- dati1973 %>%
  filter(Nome_Regione=="Calabria") 
dati_calabria1978 <- dati1978 %>%
  filter(Nome_Regione=="Calabria") 
dati_calabria1981 <- dati1981 %>%
  filter(Nome_Regione=="Calabria") 

plot(dati_calabria1973$XUTM,dati_calabria1973$YUTM,pch=20,xlab="x",ylab="y", col="green")
lines(Cal$X,Cal$Y)
title(main="Localisation of the rainfall data in the Calabria region") 
points(dati_calabria1978$XUTM,dati_calabria1978$YUTM, col="red")
points(dati_calabria1981$XUTM,dati_calabria1981$YUTM, col="blue",pch=3)
# The rainfall data seems uniformly distributed all over the region Calabria, but like in Basilicata, there are stations left in 1978 and more in 1981.


# Puglia
dati_puglia1973 <- dati1973 %>%
  filter(Nome_Regione=="Puglia") 
dati_puglia1978 <- dati1978 %>%
  filter(Nome_Regione=="Puglia") 
dati_puglia1981 <- dati1981 %>%
  filter(Nome_Regione=="Puglia") 

plot(dati_puglia1973$XUTM,dati_puglia1973$YUTM,pch=20,xlab="x",ylab="y",col="green")
lines(Cal$X,Cal$Y)
lines(Bas$X,Bas$Y)
title(main="Localisation of the rainfall data in the Puglia region") 
points(dati_puglia1978$XUTM,dati_puglia1978$YUTM, col="red")
points(dati_puglia1981$XUTM,dati_puglia1981$YUTM, col="blue",pch=3)
# There are not enough data for the Puglia's region (only 3), so we will not compute/conclude anything for this region.

#---------------------

#On theses 3 regions, through the years, we can visualize the amount of rainfall 
ggplot(dati1973) +
  geom_point(aes(XUTM,YUTM,col=totanno, fill=totanno,size=totanno))
ggplot(dati1978) +
  geom_point(aes(XUTM,YUTM,col=totanno, fill=totanno,size=totanno))
ggplot(dati1981) +
  geom_point(aes(XUTM,YUTM,col=totanno, fill=totanno,size=totanno))
#It's not always the same places where it rains the most, but the west region seems rainier than the east.


#---------------------
# We check the distribution of rainfall amount, each year, for the 3 regions.

summary(dati1973)
hist(dati1973$totanno, freq=FALSE, breaks=25,
     main = "Histogram of the rainfall data (1973)", xlab='totanno')
lines(density(dati1973$totanno))
# The distribution could look like a Gaussian one, a test would be actually useful to "confirm" it.

qqnorm(dati1973$totanno) 
qqline(dati1973$totanno, datax = FALSE, distribution = qnorm,
       probs = c(0.25, 0.75), qtype = 7) # slope > 1
shapiro.test(dati1973$totanno) #the pvalue is not high enough

# exploring the transformations allows us to improve the normality of our dataset.
# Which transformation to pick? totanno >=0

qqnorm(log(dati1973$totanno)) # qqplot is quite working
qqline(log(dati1973$totanno), datax = FALSE, distribution = qnorm,
       probs = c(0.25, 0.75), qtype = 7)
shapiro.test(log(dati1973$totanno)) #the test is better

shapiro.test(sqrt(dati1973$totanno)) #worst

shapiro.test(1/(dati1973$totanno)) #worst 

#same for 1978
summary(dati1978)
hist(dati1978$totanno, freq=FALSE, breaks=25,
     main = "Histogram of the rainfall data (1978)", xlab='totanno')
lines(density(dati1978$totanno)) # The distribution of the data looks a little bit like a gaussian distribution but is asymetric
shapiro.test(dati1978$totanno) 
shapiro.test(log(dati1978$totanno)) #great
shapiro.test(sqrt(dati1978$totanno)) #the best
shapiro.test(1/(dati1978$totanno)) 

# same for 1981
summary(dati1981)
hist(dati1981$totanno, freq=FALSE, breaks=25,
     main = "Histogram of the rainfall data (1981)", xlab='totanno')
lines(density(dati1981$totanno)) # The distribution of the data looks a little bit like a gaussian distribution but is asymetric
shapiro.test(dati1981$totanno) 
shapiro.test(log(dati1981$totanno)) #the best transformation   
shapiro.test(sqrt(dati1981$totanno)) 
shapiro.test(1/(dati1981$totanno))




# rainfall amount through the years

plot(density(dati1981$totanno), col="blue", main="Densities of rainfall amount througt the years 
    - 1973(black), 1978(red), 1981(blue) : the amount of rainfall decreases ", 
     xlab='totanno')
lines(density(dati1978$totanno), col="red")
lines(density(dati1973$totanno))
# the amount of rainfall decreases



scatterplot3d(dati1973$XUTM,dati1973$YUTM,dati1973$totanno,
              type = "h", highlight.3d = TRUE,pch = 20, 
              main = "3D representation of the distribution of the total amount of rainfall",
              xlab='x',ylab='y',zlab='Rainfall' )

# We can observe that two different regions emerge depending on the amount of rainfall on 1973.
# The first one have higher value on coordinate y and also higher values of rainfall in a darker light.
# The other one with smaller on coordinate y and smaller values of rainfall in light red

scatterplot3d( dati1978$XUTM, dati1978$YUTM, dati1978$totanno,
               type = "h", highlight.3d = TRUE,pch = 20, 
               main = "3D representation of the distribution of the total amount of rainfall",
               xlab='x',ylab='y',zlab='Rainfall' )

# Same for 1978

scatterplot3d( dati1981$XUTM, dati1981$YUTM, dati1981$totanno,
               type = "h", highlight.3d = TRUE,pch = 20, 
               main = "3D representation of the distribution of the total amount of rainfall",
               xlab='x',ylab='y',zlab='Rainfall' )

# Same for 1981



# 2) Check the Spatial dependence and variograms, through the years
dati1973_spatial<-dati1973
coordinates(dati1973_spatial) <- ~ XUTM + YUTM
v1973 <- variog(dati1973_spatial, 
                coords = cbind(dati1973_spatial$XUTM,dati1973_spatial$YUTM), 
                data = dati1973_spatial$totanno)
plot(v1973,type="b")

dati1978_spatial<-dati1978
coordinates(dati1978_spatial) <- ~ XUTM + YUTM
v1978 <- variog(dati1978_spatial, coords = cbind(dati1978_spatial$XUTM,dati1978_spatial$YUTM), data = dati1978_spatial$totanno)
plot(v1978,type="b")

dati1981_spatial<-dati1981
coordinates(dati1981_spatial) <- ~ XUTM + YUTM
v1981 <- variog(dati1981_spatial, coords = cbind(dati1981_spatial$XUTM,dati1981_spatial$YUTM), data = dati1981_spatial$totanno)
plot(v1981,type="b")

# At first the shape of the variograms don't seem to give us a lot of information
# The variograms values decrease a lot when the distance become really high, which is non intuitive
# Actually this increasing dependence seems due to the elevation :
scatterplot3d(dati1973$XUTM,dati1973$YUTM,dati1973$elevation,
              type = "h", highlight.3d = TRUE,pch = 20,
              main = "3D representation of the distribution of the elevation",
              xlab='x',ylab='y',zlab='elevation' )
# In fact, they are different hills in the map.
# Logically two places with the same elevation are supposed to have the same kind of weather,
# according to the previous question (high correlation between the amount of rainfall and the elevation).



# We consider the distance inferior to 180000:
plot(v1973,type="b", max.dist = 180000)
plot(v1978,type="b", max.dist = 180000)
plot(v1981,type="b", max.dist = 180000)
# The functions are now growing so we can suppose spatial correlation in the data
# The shape of this variogram look like a Stationary process: it seems to have a sill
# Furthermore there is an important nugget, it could imply that there is an important measurement error



# We study the relationship between the rainfall amount and the elevation 
# We will test this on the all data set, through the years

plot(dati1973$Quota, dati1973$totanno, pch=20,
     xlab="elevation", ylab="rainfall amount",
     main="Relationship between rainfall amount and elevation")
# The plot suggest a linear relationship between rainfall amount and elevation
# despite we do not have a lot of information for over 1000 meters
# We will study by a simple linear regression.
p <- lm(totanno~Quota, dati1973)
summary(p)
cor.test(dati1973$totanno,dati1973$Quota) 
# The linear regression is globally significant at 1% threshold (for the F-statistics)
# So we can think about a positive linear relationship between rainfall amount and elevation 
abline(p, col = "red")


plot(dati1978$Quota, dati1978$totanno, pch=20,
     xlab="elevation", ylab="rainfall amount",
     main="Relationship between rainfall amount and elevation")
p <- lm(totanno~Quota, dati1978)
summary(p)
cor.test(dati1978$totanno,dati1978$Quota) 
abline(p, col = "red")
#same as 1973
plot(dati1981$Quota, dati1981$totanno, pch=20,
     xlab="elevation", ylab="rainfall amount",
     main="Relationship between rainfall amount and elevation")
p <- lm(totanno~Quota, dati1981)
summary(p)
cor.test(dati1981$totanno,dati1981$Quota) 
abline(p, col = "red")
#same as 1973 and 1978


### Question 2: Using the IDW estimator, map rainfall values on the grid recorded in the database quota.
# Basilicata & Calabria

dati1973_basilicata_calabria <- dati1973[dati1973$Nome_Regione=="Basilicata" | dati1973$Nome_Regione == "Calabria",]
dati1978_basilicata_calabria <- dati1978[dati1978$Nome_Regione=="Basilicata" | dati1978$Nome_Regione == "Calabria",]
dati1981_basilicata_calabria <- dati1981[dati1981$Nome_Regione=="Basilicata" | dati1981$Nome_Regione == "Calabria",]

# Create the grid
colnames(Basquota32)<-colnames(Calabriaquota32)
grid<-rbind(Basquota32,Calabriaquota32)
colnames(grid) <- c("ID", "XUTM", "YUTM", "quota")


# Plot the grid
plot(grid$X,grid$Y,pch=20, cex=0.1)
points(Bas$XUTM,Bas$YUTM,col=20)
points(Cal$XUTM,Cal$YUTM,col=20)
points(dati1973$XUTM,dati1973$YUTM,pch=20,col="green")
title(main="Localisation of the stations") 
points(dati1978$XUTM,dati1978$YUTM, col="red")
points(dati1981$XUTM,dati1981$YUTM, col="blue",pch=3)

# Predict data points on the grid 
idw_1973basilicata_calabria <- idw(totanno~1, locations=~XUTM+YUTM, dati1973_basilicata_calabria, grid)
idw_1978basilicata_calabria <- idw(totanno~1, locations=~XUTM+YUTM, dati1978_basilicata_calabria, grid)
idw_1981basilicata_calabria <- idw(totanno~1, locations=~XUTM+YUTM, dati1981_basilicata_calabria, grid)

ggplot(idw_1973basilicata_calabria) +
  geom_point(aes(XUTM,YUTM,col=var1.pred, fill=var1.pred,size=var1.pred))
ggplot(idw_1978basilicata_calabria) +
  geom_point(aes(XUTM,YUTM,col=var1.pred, fill=var1.pred,size=var1.pred))
ggplot(idw_1981basilicata_calabria) +
  geom_point(aes(XUTM,YUTM,col=var1.pred, fill=var1.pred,size=var1.pred))


### Question 3: Choose the optimal value for p in the IDW estimator using cross-validation.

p<-seq(0.5,5,by=0.5)

MSE1973<-rep(0,length(p))
MSE1978<-rep(0,length(p))
MSE1981<-rep(0,length(p))
j=3
for(j in 1:length(p)){
  pred1973<-rep(0,nrow(dati1973))
  pred1978<-rep(0,nrow(dati1978))
  pred1981<-rep(0,nrow(dati1981))
  
  # Leave-one out cross validation 
  for(i in 1:nrow(dati1973)){
    cc1973<-dati1973[-i,]
    coordinates(cc1973)<-~XUTM+YUTM
    cc1973_1<-dati1973[i,c("XUTM","YUTM")]
    coordinates(cc1973_1)<-~XUTM+YUTM
    pred1973[i]<-idw(totanno~1,cc1973,newdata=cc1973_1,idp=p[j])$var1.pred
  }
  
  for(i in 1:nrow(dati1978)){
    cc1978<-dati1978[-i,]
    coordinates(cc1978)<-~XUTM+YUTM
    cc1978_1<-dati1978[i,c("XUTM","YUTM")]
    coordinates(cc1978_1)<-~XUTM+YUTM
    pred1978[i]<-idw(totanno~1,cc1978,newdata=cc1978_1,idp=p[j])$var1.pred
  }
  
  for(i in 1:nrow(dati1981)){
    cc1981<-dati1981[-i,]
    coordinates(cc1981)<-~XUTM+YUTM
    cc1981_1<-dati1981[i,c("XUTM","YUTM")]
    coordinates(cc1981_1)<-~XUTM+YUTM
    pred1981[i]<-idw(totanno~1,cc1981,newdata=cc1981_1,idp=p[j])$var1.pred
  }
  
  MSE1973[j]<-mean((pred1973-dati1973$totanno)^2)
  MSE1978[j]<-mean((pred1978-dati1978$totanno)^2)
  MSE1981[j]<-mean((pred1981-dati1981$totanno)^2)
  
}

# We choose the lower value Of MSE that we have calculated
w1973<-which(MSE1973==min(MSE1973))
w1978<-which(MSE1978==min(MSE1978))
w1981<-which(MSE1981==min(MSE1981))

plot(p,MSE1973)
points(p[w1973],MSE1973[w1973],pch=20,col=2)
p_opt1973<-p[w1973]

plot(p,MSE1978)
points(p[w1978],MSE1978[w1978],pch=20,col=2)
p_opt1978<-p[w1978]

plot(p,MSE1981)
points(p[w1981],MSE1981[w1981],pch=20,col=2)
p_opt1981<-p[w1981]

# The optimal values for p is:
p_opt1973
p_opt1978
p_opt1981

